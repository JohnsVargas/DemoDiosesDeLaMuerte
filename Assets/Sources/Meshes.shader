﻿Shader "MyShaders/Meshes"
{
	SubShader
	{
		Tags{
		"RenderType" = "Transparent"
		"Queue" = "Transparent"
	}
		LOD 200

		// store depth buffer
		ZWrite On
		ZTest LEqual
		Cull Back
		Blend One Zero

		Pass{
		CGPROGRAM
		:
	VertexShaderOutput myVertexShader(VertexShaderInput vertexIn)
	{
		:
		// if theta > 0, it's a front face
		float lightLevel = (cos_theta >= 0) ? cos_theta / 2 : 0;
		float4 colorOut = float4(0, 0, lightLevel, 0.3);
		:
	}
	:
	ENDCG
	}


		// Now render FRONT+BACK faces
		Cull Off
		ZWrite Off
		ZTest Always
		Blend One One

		Pass{

		CGPROGRAM
		:
	VertexShaderOutput myVertexShader(VertexShaderInput vertexIn)
	{
		:
		// if theta < 0, it's a back face
		float lightLevel = (cos_theta < 0) ? -cos_theta / 4 : 0;
		float4 colorOut = float4(lightLevel, 0, 0, 0.15);
		:
	}
	:
	ENDCG
	}
	} // Subshader
}