﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgruondParallax : MonoBehaviour {

	public float speed = 0.2f;
    public Transform transformPlayer;
    private Material BackGround;
    float posInitial, posEnd;
	void Awake(){
		BackGround = GetComponent<MeshRenderer> ().material;
		BackGround.SetTextureOffset ("_MainTex",Vector2.zero);
        transformPlayer = GameObject.Find("PlayerCapsule").GetComponent<Transform>();
        posInitial = 0;

    }

	// Update is called once per frame
	void Update () {
        
        float move = Input.GetAxis("Horizontal");
        //move = transformPlayer.velocity.x;
        posEnd = transformPlayer.position.x;
        if(posInitial.ToString("#.00") != posEnd.ToString("#.00"))
        {
            Vector2 newOffset = BackGround.GetTextureOffset("_MainTex");
            newOffset.x += speed * move * Time.deltaTime;
            BackGround.SetTextureOffset("_MainTex", newOffset);
        }
        posInitial = transformPlayer.position.x;
    }
}
