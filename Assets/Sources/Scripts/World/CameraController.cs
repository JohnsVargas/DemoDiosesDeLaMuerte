﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
	public GameObject player;
	private float offset;
	private Vector3 temp;
	// Use this for initialization
	void Start () {
        player = GameObject.Find("PlayerCapsule");
		temp = transform.position - player.transform.position;
		offset = temp.x;

	}
	
	// Update is called once per frame
	void Update () {
		transform.position =  new Vector3((player.transform.position.x + offset)
			,transform.position.y
			,transform.position.z);
	}
}
                    