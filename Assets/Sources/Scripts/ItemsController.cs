﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsController : MonoBehaviour {
    public enum Items {Magic,Life,PieceLife};
    public Items item;
    private Animator animatorOrbes;
	// Use this for initialization
	void Start () {
        animatorOrbes = GetComponent<Animator>();
        setAnimationOrbe(item);
        transform.position = new Vector3(transform.position.x, transform.position.y, 12.55f);
	}

    private void setAnimationOrbe(Items Newitem)
    {
        int value;
        switch (Newitem)
        {
            
            case Items.Life:
                value = 0;
                break;
            case Items.Magic:
                value =  1;
                break;
            case Items.PieceLife:
                value = 2;
                break;
            default:
                value = 0;
                break;
        }
        animatorOrbes.SetInteger("change", value);
    }
}
