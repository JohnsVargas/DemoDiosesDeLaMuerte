﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

	public float speed;
	public float ForceJump;
	public bool grounded = true;
	public Transform groundCheck;
	public LayerMask GroundMask;
    public GameObject PlayerBody;
    public GameObject guadaña;
    float move, velocity;
    public float lifePlayer,totalLifePlayer;
    private Rigidbody rigibodyPlayer;
    private Animator animatorPlayer;
    AnimatorStateInfo animatorStateInfoPlayer;
    private Image barLife;
    private AudioSource audioFBX;
    public bool flip;
    public AudioClip[] soundsEffect;
    // Use this for initialization
    void Start () {
		rigibodyPlayer = GetComponent<Rigidbody> ();
		animatorPlayer = PlayerBody.GetComponent<Animator> ();
        guadaña.GetComponent<BoxCollider>().enabled = false;
        audioFBX = GameObject.Find("FBX").GetComponent<AudioSource>();
        resetPlayer();
        flip = false;
	}

    void resetPlayer() {
        totalLifePlayer = SaveManager.instance.GetLifeTotal();
        lifePlayer = SaveManager.instance.GetLifeInGame();
        SetLifeAmount();
    }
	// Update is called once per frame
	void Update () {
        CheckFlip();
        move = Input.GetAxis("Horizontal");
		rigibodyPlayer.velocity = new Vector3 (move * speed*Time.deltaTime, rigibodyPlayer.velocity.y, rigibodyPlayer.velocity.z);
        velocity = Mathf.Abs(rigibodyPlayer.velocity.x);
		animatorPlayer.SetFloat ("Speed", velocity);
       
        if (Input.GetKeyDown (KeyCode.Space) && grounded) {
			rigibodyPlayer.AddForce (new Vector3 (0f, ForceJump*Time.deltaTime, 0f),ForceMode.Impulse);
			grounded = false;
		}

        animatorStateInfoPlayer = animatorPlayer.GetCurrentAnimatorStateInfo(0);
        if (Input.GetKeyDown(KeyCode.Z) && !animatorStateInfoPlayer.IsName("Attack"))
        {
            animatorPlayer.SetTrigger("Attack");
            audioFBX.PlayOneShot(soundsEffect[0]);

        }
        
        guadaña.GetComponent<BoxCollider>().enabled = animatorStateInfoPlayer.IsName("Attack") ;

    }

	void OnCollisionEnter(Collision obj){
		if(obj.gameObject.layer == 8)
			grounded = true;
        if (obj.gameObject.CompareTag("LightWin"))
        {
            GamePlayManager.instance.LoseAndWin("Next Level!!", GamePlayManager.StateGame.Win);
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.CompareTag("Orbes"))
        {
            ItemsController item = collider.transform.GetComponent<ItemsController>();
            if (ItemsController.Items.Magic == item.item)
            {
                audioFBX.PlayOneShot(soundsEffect[1]);
                ManagersItems.instance.SetOrbesMagic(1);
            }


            else if (ItemsController.Items.Life == item.item) {
                audioFBX.PlayOneShot(soundsEffect[1]);
                ManagersItems.instance.SetOrbesLife(1);
            }
                ManagersItems.instance.SetOrbesLife(1);

            Destroy(collider.gameObject);
        }
        else if (collider.gameObject.CompareTag("EnemyDamage"))
        {
            EnemyDamage enemyDemage=collider.GetComponent<EnemyDamage>();
            UpdateLifePlayer( -1*enemyDemage.GetDamageToPlayer());
        }
        else if (collider.gameObject.CompareTag("BulletDamage"))
        {
            UpdateLifePlayer(-1);
            ObjectPool.instance.PoolGameObject(collider.gameObject);
        }
        else if (collider.gameObject.CompareTag("EnemyLava"))
        {
            UpdateLifePlayer(totalLifePlayer*-1);
            ObjectPool.instance.PoolGameObject(collider.gameObject);
        }
        else if (collider.gameObject.CompareTag("Trampas"))
        {
            UpdateLifePlayer(-1);
            ///transform.Translate(-10f, 0f, 0f);
            rigibodyPlayer.AddForce(Vector3.right* -100, ForceMode.Impulse);
            //ObjectPool.instance.PoolGameObject(collider.gameObject);
        }
    }
    public void UpdateLifePlayer(float value)
    {
        lifePlayer = lifePlayer + value;
        lifePlayer = lifePlayer > totalLifePlayer ? totalLifePlayer :  lifePlayer;
        if (barLife == null)
            barLife = GameObject.Find("BarLife").GetComponent<Image>();

        barLife.fillAmount = (lifePlayer / totalLifePlayer);
        if (barLife.fillAmount <= 0.2)
            barLife.color = Color.red;
        if (barLife.fillAmount == 0)
        {
            GamePlayManager.instance.LoseAndWin("Loser!!",GamePlayManager.StateGame.Lose);
            lifePlayer = 0;
            //Debug.Log("Estas Muerto");//currentState = stateEnemys.Die;
        }
        SaveManager.instance.SetLifeInGame(lifePlayer);
            
    }
    public void SetLifeAmount() {
        if (barLife == null)
            barLife = GameObject.Find("BarLife").GetComponent<Image>();

        barLife.fillAmount = (lifePlayer / totalLifePlayer);
        if (barLife.fillAmount <= 0.2)
            barLife.color = Color.red;
    }
    void CheckFlip()
    {
        if (Time.timeScale != 0)
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow))
                flip = true;

            if (Input.GetKeyDown(KeyCode.RightArrow))
                flip = false;

            PlayerBody.transform.rotation = Quaternion.Euler(0, flip ? -90 : 90, 0);
        }
        
    }
}
