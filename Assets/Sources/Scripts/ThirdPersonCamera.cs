﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCamera : MonoBehaviour {

	public Transform target;
	public float smooth = 0.3f;
	public float distance = 5.0f;
	private float yVelocity = 0f;
	public float height = 3.5f;
	// Update is called once per frame
	void Update () {
		float yAngule = Mathf.SmoothDampAngle (transform.eulerAngles.y,target.eulerAngles.y+20f,ref yVelocity,smooth);

		Vector3 position = target.position;
		//position +=new Vector3(0,height,-distance); //asi solo lo seguiria
		position += Quaternion.Euler (0,yAngule,0)* new Vector3(0,height,-distance); // sigue al persoje desde la espalda
		transform.position = position;
		transform.LookAt (target);
	}
}
