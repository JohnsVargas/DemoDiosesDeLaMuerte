﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;

public class ObjectPool : MonoBehaviour {
    public static ObjectPool instance;
    [System.Serializable]
    public struct PrefabPoll
    {
        public GameObject prefab;
        public int amountInBuffer;
    };
    public PrefabPoll[] prefabs;
    public List<GameObject>[] generalPool;
    protected GameObject containerObject;
	// Use this for initialization
	void Awake () {
        Profiler.BeginSample("PoolingObjects");
        instance = this;
        containerObject = new GameObject("ObjectPool");
        generalPool = new List<GameObject>[prefabs.Length];
        int index = 0;
        foreach (PrefabPoll objectprefab in prefabs)
        {
            generalPool[index] = new List<GameObject>();
            for (int i = 0; i < objectprefab.amountInBuffer; i++)
            {
                GameObject temp = Instantiate(objectprefab.prefab);
                temp.name = objectprefab.prefab.name;
                PoolGameObject(temp);
            }
            index++;
        }
        Profiler.EndSample();
    }
    public void PoolGameObject(GameObject obj)
    {
        for (int i = 0; i < prefabs.Length; i++)
        {
            if (prefabs[i].prefab.name == obj.name)
            {
                obj.SetActive(false);
                obj.transform.parent = containerObject.transform;
                obj.transform.position = containerObject.transform.position;
                generalPool[i].Add(obj);
            }
        }
    }
    public GameObject GetGameObjectOffType(string obectType,bool onlyPooled)
    {
        for (int i = 0; i < prefabs.Length; i++)
        {
            GameObject prefab = prefabs[i].prefab;
            if (prefab.name == obectType)
            {
                if (generalPool[i].Count > 0)
                {
                    GameObject pooledObject = generalPool[i][0];
                    pooledObject.transform.parent = null;
                    generalPool[i].RemoveAt(0);
                    pooledObject.SetActive(true);
                    return pooledObject;

                }
            }
            else if (!onlyPooled)
            {
                return Instantiate(prefabs[i].prefab);
            }
            //break;
        }
        return null;
    }
}
