﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIManager : MonoBehaviour {
    public static UIManager instance;
    private GameObject PausePanel;
    private GameObject HUDPanel;
    private GameObject IncioPanel;
    private GameObject PanelReiniciar;
    private Text txtPanelReiniciarTitle;
    public enum PanelesUI { IncioPanel,PausePanel, HUDPanel, PanelReiniciar };
    // Use this for initialization
    void Awake () {
        instance = this;
        LoadPanels();

    }
	
    private void LoadPanels()
    {
        HUDPanel = GameObject.Find("HUDPanel");
        PausePanel = GameObject.Find("PausePanel");
        IncioPanel = GameObject.Find("InicioPanel");
        PanelReiniciar  = GameObject.Find("PanelReiniciar");
        txtPanelReiniciarTitle = GameObject.Find("TextReiniciarTitle").GetComponent<Text>();
    }

    private void HidePanels()
    {
        HUDPanel.SetActive(false) ;
        PausePanel.SetActive(false);
        IncioPanel.SetActive(false);
        PanelReiniciar.SetActive(false);
    }
    public void ShowPanel(PanelesUI panel)
    {
        HidePanels();
        if (panel == PanelesUI.IncioPanel)
            IncioPanel.SetActive(true);
        if (panel == PanelesUI.PausePanel)
            PausePanel.SetActive(true);
        if (panel == PanelesUI.HUDPanel)
            HUDPanel.SetActive(true);
        if (panel == PanelesUI.PanelReiniciar)
            PanelReiniciar.SetActive(true);

    }

    public void SetPanelReiniciar(string title)
    {
        txtPanelReiniciarTitle.text = title;
    }
}
