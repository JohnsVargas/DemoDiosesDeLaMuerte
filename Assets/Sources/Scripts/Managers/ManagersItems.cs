﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManagersItems : MonoBehaviour
{
    public static ManagersItems instance;
    public int OrbesLife;
    public int OrbesMagic;
    public int OrbesPiecesLife;
    public Text txtNumMagic;
    private Image barLife;
    private PlayerController playerController;
    void Awake()
    {
        instance = this;
        if (PlayerPrefs.HasKey("OrbesMagic"))
        {
            GetOrbes();
        }
        else
        {
            OrbesLife = 0;
            OrbesMagic = 0;
            OrbesPiecesLife = 0;
        }

    }

    public int GetOrbesPiecesLife()
    {
        return OrbesPiecesLife;
    }
    public int GetOrbesMagic()
    {
        return OrbesMagic;
    }

    public int GetOrbesLife()
    {
        return OrbesLife;
    }

    public void SetOrbesMagic(int value)
    {
        if (txtNumMagic == null)
            txtNumMagic = GameObject.Find("txtNumMagic").GetComponent<Text>();

        OrbesMagic += value;
        txtNumMagic.text = string.Format("x{0}",OrbesMagic.ToString());
        txtNumMagic.text = string.Format("x{0}",OrbesMagic.ToString());
        
    }
    public void SetTxtOrbesMagic() {
        if (txtNumMagic == null)
            txtNumMagic = GameObject.Find("txtNumMagic").GetComponent<Text>();
        txtNumMagic.text = string.Format("x{0}", OrbesMagic.ToString());
    }
    public void SetOrbesLife(int value)
    {
        if(playerController == null)
            playerController = GameObject.Find("PlayerCapsule").GetComponent<PlayerController>();
        playerController.UpdateLifePlayer(value);

    }

    public void SaveOrbes() {
        PlayerPrefs.SetInt("OrbesLife", OrbesLife);
        PlayerPrefs.SetInt("OrbesMagic", OrbesMagic);
        PlayerPrefs.SetInt("OrbesPiecesLife", OrbesPiecesLife);
    }
    public void GetOrbes()
    {
        OrbesLife = PlayerPrefs.GetInt("OrbesLife");
        OrbesMagic = PlayerPrefs.GetInt("OrbesMagic");
        OrbesPiecesLife = PlayerPrefs.GetInt("OrbesPiecesLife");
    }
}
