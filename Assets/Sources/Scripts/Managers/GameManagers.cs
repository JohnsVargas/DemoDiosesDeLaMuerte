﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagers : MonoBehaviour {

    public static GameManagers gameManagers;

    public void Awake()
    {
        if (gameManagers == null)
        {
            DontDestroyOnLoad(gameObject);
            gameManagers = this;
        }
        else if (gameManagers != this)
        {
            Destroy(gameObject);
        }
    }
}
