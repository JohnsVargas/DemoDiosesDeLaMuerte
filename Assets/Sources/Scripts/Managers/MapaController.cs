﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapaController : MonoBehaviour {

    public void GotoScene(string sceneName)
    {
        ScenesManagers.instance.SceneGoto(sceneName);
    }
    public void Salir() {
        Application.Quit();
    }
}
