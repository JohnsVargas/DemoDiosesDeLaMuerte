﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesManagers : MonoBehaviour {
    public static ScenesManagers instance;
    void Awake()
    {
        instance = this;
    }
    public void SceneGoto(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
}
