using System.Collections.Generic;
using System.IO; //	Cloud
using UnityEngine;

public class Datamanager : MonoBehaviour {

	//	P.S.:
	//	Search about Singleton
	//	collision.gameobject.SendMessage(Die); /*** No Es Instantaneo ***/
	//	collision.gameobject.GetComoponent<Example>().Die();
	//	Puede ser estática o una referencia estática

	//	Just one static Datamanager in the scene
	public  static Datamanager instance;


	public string filename;

	private StreamReader sr;
	private StreamWriter sw;
	private string contentToReadWrite;


	public Enemy enemy;
    public List<Enemy> enemies;

    void Awake(){
		instance = this;
		LoadData(filename);
	}


	public void SaveData(string path){

		/*		Gardado de datos local usando PlayerPrefs
		//	Saving the data
		PlayerPrefs.SetInt ("EnemyArmor",enemy.armor);
		PlayerPrefs.SetInt ("EnemyLevel",enemy.level);
		PlayerPrefs.SetString ("EnemyName",enemy.name);

		//	Forcing to save (not wait to quit app)
		PlayerPrefs.Save();
		//	To delete all keys
		//	PlayerPrefs.DeleteAll();
		*/


		/*		Gardado de datos usando archivos	*/
		sw = new StreamWriter(Application.persistentDataPath + "/" + path , false);
		contentToReadWrite = "";
		contentToReadWrite = JsonUtility.ToJson (enemy);
		sw.Write (contentToReadWrite);
		sw.Close ();
		Debug.Log ("Saved to: " + Application.persistentDataPath + "/" + path);

	}

	public void LoadData(string path){
		enemy = new Enemy();
        /*		Gardado de datos local usando PlayerPrefs
		//	Carga de datos usando player prefs
		//	Si tiene uno tiene todos
		if (PlayerPrefs.HasKey ("EnemyArmor")) {
			enemy.armor = PlayerPrefs.GetInt ("EnemyArmor");
			enemy.level = PlayerPrefs.GetInt ("EnemyLevel");
			enemy.name = PlayerPrefs.GetString ("EnemyName");
		} else 
			enemy.armor = 3;
			enemy.level = 1;
			enemy.name = "DefaultEnemy";
		}*/



        /*		Gardado de datos usando archivos	*/
        Debug.Log(Application.persistentDataPath + "/" + path); 
		if (File.Exists (Application.persistentDataPath + "/" + path)) {
			sr = new StreamReader (Application.persistentDataPath + "/" + path);
			contentToReadWrite = "";
			contentToReadWrite = sr.ReadToEnd ();
            //(Player)JsonUtility.FromJson(jsonString, typeof(Player));
            //enemies = JsonUtility.FromJson<List<Enemy>>(contentToReadWrite);
            enemy = (Enemy)JsonUtility.FromJson(contentToReadWrite,typeof(Enemy));
			sr.Close ();
			Debug.Log ("Loaded from: " + Application.persistentDataPath + "/" + path);
		} else {
            enemy.damage = 0.5f;
            enemy.typeEnemy = 1;
			enemy.nivel = 1;
			enemy.name = "DefaultEnemy";
            enemy.life = 2.0f;
        }

	}

	public void DeleteData(string path){
		if (File.Exists (Application.persistentDataPath + "/" + path)) {
			File.Delete (Application.persistentDataPath + "/" + path);
			Debug.Log ("Deleted from: " + Application.persistentDataPath + "/" + path);
		}
	}

	void Update(){
		if(Input.GetKeyDown(KeyCode.S)){
			SaveData(filename);
		}
		if(Input.GetKeyDown(KeyCode.D)){
			DeleteData(filename);
		}

	}

}
