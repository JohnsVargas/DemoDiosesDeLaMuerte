﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GamePlayManager : MonoBehaviour {
    public static GamePlayManager instance;
    public enum StateGame {Incio, Game,Pause,Lose,Win};
    public StateGame stateGame;
    private float tempTimeScale;
    private bool isPaused;

    // Use this for initialization
    void Awake()
    {
        instance = this;
    }
    void Start () {
        stateGame = StateGame.Incio;
        ManagersItems.instance.SetTxtOrbesMagic();
        isPaused = false;
	}
	
	// Update is called once per frame
	void Update () {
        switch (stateGame)
        {
            case StateGame.Incio:
                #region Incio
                if (Time.timeScale != 0)
                {
                    tempTimeScale = Time.timeScale;
                    Time.timeScale = 0;
                    UIManager.instance.ShowPanel(UIManager.PanelesUI.IncioPanel);
                }
                #endregion
                break;
            case StateGame.Game:
                #region Game
                if (Input.GetKeyDown(KeyCode.Escape))
                    Pause();
                #endregion
                break;

            case StateGame.Pause:
                #region Pause
                #endregion
                break;
            case StateGame.Lose:
                #region Lose
                #endregion
                break;
            case StateGame.Win:
                #region Win
                
                #endregion
                break;
            default:
                break;
        }
	}

    public void Comensar()
    {
        stateGame = StateGame.Game;
        Time.timeScale = tempTimeScale;
        UIManager.instance.ShowPanel(UIManager.PanelesUI.HUDPanel);
    }

    public void Pause()
    {
        isPaused = !isPaused;
        if (isPaused)
        {
            stateGame = StateGame.Pause;
            tempTimeScale = Time.timeScale;
            Time.timeScale = 0;
            UIManager.instance.ShowPanel(UIManager.PanelesUI.PausePanel);
        }
        else {
            stateGame = StateGame.Game;
            Time.timeScale = tempTimeScale;
            UIManager.instance.ShowPanel(UIManager.PanelesUI.HUDPanel);
        }
        
    }
    public void LoseAndWin(string title ,StateGame Newstate )
    {
        stateGame = Newstate;
        tempTimeScale = Time.timeScale;
        Time.timeScale = 0;
        
        UIManager.instance.ShowPanel(UIManager.PanelesUI.PanelReiniciar);
        UIManager.instance.SetPanelReiniciar(title);
    }
    public void  GoToMenu() {
        SceneManager.LoadScene("Mapa");
    }

    public void Continuar(int level)
    {
        Time.timeScale = tempTimeScale;
        //Guardar Variables
        if (stateGame == StateGame.Win) {
            ManagersItems.instance.SaveOrbes();
            SaveManager.instance.SaveDataPlayer();
            SceneManager.LoadScene("Mundo" + level.ToString());
            SaveManager.instance.SaveWorld(level-1);
        }
        else{
            SceneManager.LoadScene("Mundo" + (level-1).ToString());
        }
        
    }
}
