﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : MonoBehaviour {
    public static SaveManager instance;

    public float LifeInGame;
    public float LifeTotal;
    public int GuadanaLevel;
    public float GuadanaDamage;
    public int Mundo1;
    public int Mundo2;
    // Use this for initialization
    void Awake() {
        instance = this;
        if (PlayerPrefs.HasKey("LifeTotal"))
        {
            GetDataPlayer();
        }
        else
        {
            LifeTotal = 3;
            LifeInGame = 3;
            GuadanaLevel = 1;
            GuadanaDamage = 0.5f;
            Mundo1 = 0;
            Mundo2 = 0;
        }
    }
    public float GetLifeInGame() {
        return LifeInGame;
    }
    public void SetLifeInGame(float Newlife)
    {
        LifeInGame = Newlife;
    }
    public float GetLifeTotal()
    {
        return LifeTotal;
    }
    public int GetGuadanaLevel()
    {
        return GuadanaLevel;
    }
    public float GetGuadanaDamage()
    {
        return GuadanaDamage;
    }
    public void SaveDataPlayer()
    {
        PlayerPrefs.SetFloat("LifeTotal", LifeTotal);
        PlayerPrefs.SetFloat("LifeInGame", LifeInGame);
        PlayerPrefs.SetInt("GuadanaLevel", GuadanaLevel);
        PlayerPrefs.SetFloat("GuadanaDamage", GuadanaDamage);
    }
    public void GetDataPlayer()
    {
        LifeTotal = PlayerPrefs.GetFloat("LifeTotal");
        LifeInGame = PlayerPrefs.GetFloat("LifeInGame");
        GuadanaLevel = PlayerPrefs.GetInt("GuadanaLevel");
        GuadanaDamage = PlayerPrefs.GetFloat("GuadanaDamage");
        Mundo1 = PlayerPrefs.GetInt("Mundo1");
        Mundo2 = PlayerPrefs.GetInt("Mundo2");
    }
    public void SaveWorld(int numWolrd)
    {
        switch (numWolrd) {
            case 1:
                PlayerPrefs.SetFloat("Mundo1", 1);
                break;
            case 2:
                PlayerPrefs.SetFloat("Mundo2", 1);
                break;
        }
    }
}
