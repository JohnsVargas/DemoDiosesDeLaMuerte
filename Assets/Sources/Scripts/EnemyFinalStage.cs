﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFinalStage : MonoBehaviour {

    // Use this for initialization
    public enum StateStageEnemy { Deactivate, Start,Current,End}
    StateStageEnemy state;
    private Animator animatorStageFinal;
    private AnimatorStateInfo animatorStageFinalInfo;
    private EnemyOneFinal enemyFinalController;
	void Start () {
        state = StateStageEnemy.Deactivate;
        enemyFinalController = GameObject.Find("Golem_FinalEnemy").GetComponent<EnemyOneFinal>();
        animatorStageFinal = GameObject.Find("EnemyFinalStage").GetComponent<Animator>();

    }
	
	// Update is called once per frame
	void Update () {
        switch (state)
        {
            case StateStageEnemy.Deactivate:
                
                break;
            case StateStageEnemy.Start:
                enemyFinalController.IsActive = true;
                if (enemyFinalController.stateEnamy == EnemyOneFinal.StateEnemyFinalOne.Idle)
                    state = StateStageEnemy.Current;
                break;
            case StateStageEnemy.Current:
                if (enemyFinalController.stateEnamy == EnemyOneFinal.StateEnemyFinalOne.Die)
                {
                    state = StateStageEnemy.End;
                    animatorStageFinal.SetTrigger("End");
                }
                    
                break;
            case StateStageEnemy.End:
                break;
        }
	}
    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.transform.CompareTag("Player"))
        {
            gameObject.GetComponent<BoxCollider>().enabled = false;
            state = StateStageEnemy.Start;
            animatorStageFinal.SetTrigger("Start");
        }

    }
}
