﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyOneFinal : MonoBehaviour {
    Rigidbody rigibodyGolem;
    public float secondsCounter = 0;
    float secondsToCount = 2.5f;
    public bool isjumping = false;
    public enum StateEnemyFinalOne { Deactivate, Idle,JumpingLeft,JumpingRight, JumpingRightTwo, Tackle,Confuse, BackToIdle, Die }
    public StateEnemyFinalOne stateEnamy;
    public float distanceToCrash;
    public float distanceInitial;
    Animator animGolem;
    bool justOne;
    public bool IsActive = false;
    AnimatorStateInfo animatorStateInfoGolem;

    float LifeTotal = 10;
    // Use this for initialization
    void Start () {
        rigibodyGolem = GetComponent<Rigidbody>();
        animGolem = GetComponent<Animator>();
        stateEnamy = StateEnemyFinalOne.Idle;
        transform.right = Vector3.forward;
        distanceInitial = transform.position.x;
        distanceToCrash = transform.position.x - 15;
        justOne = true;
    }
	
	// Update is called once per frame
	void Update () {
        if (IsActive)
        {
            secondsCounter += Time.deltaTime;
            switch (stateEnamy)
            {
                case StateEnemyFinalOne.Idle:
                    #region Idle
                    if (secondsCounter > secondsToCount && !isjumping)
                    {
                        rigibodyGolem.AddForce(Vector3.up * 10, ForceMode.Impulse);
                        rigibodyGolem.AddForce(Vector3.left * 10, ForceMode.Impulse);
                        secondsCounter = 0;
                        isjumping = true;
                        stateEnamy = StateEnemyFinalOne.JumpingLeft;
                        secondsToCount = 5f;
                        animGolem.SetTrigger("ToBacck");
                    }
                    #endregion
                    break;
                case StateEnemyFinalOne.JumpingLeft:
                    #region JumpingLeft
                    if (!isjumping)
                    {
                        transform.Rotate(Vector3.up*-1, -55 * Time.deltaTime);
                    }

                    if (secondsCounter > secondsToCount && !isjumping)
                    {
                        animGolem.SetTrigger("ReturnIdle");
                        transform.right = Vector3.forward * -1;
                        rigibodyGolem.AddForce(Vector3.up * 10, ForceMode.Impulse);
                        rigibodyGolem.AddForce(Vector3.right * 10, ForceMode.Impulse);
                        secondsCounter = 0;
                        isjumping = true;
                        secondsToCount = 5f;
                        stateEnamy = StateEnemyFinalOne.JumpingRight;
                    }
                    #endregion
                    break;
                case StateEnemyFinalOne.JumpingRight:
                    #region JumpingRight
                    if (!isjumping && secondsCounter < secondsToCount)
                    {
                        transform.Rotate(Vector3.up, 55 * Time.deltaTime);
                        if (justOne)
                        {
                            animGolem.SetTrigger("ToBacck");
                            justOne = false;
                        }

                    }
                    if (secondsCounter > secondsToCount)
                    {
                        animGolem.SetTrigger("ReturnIdle");
                        transform.right = Vector3.forward;

                        stateEnamy = StateEnemyFinalOne.JumpingRightTwo;

                        rigibodyGolem.AddForce(Vector3.up * 10, ForceMode.Impulse);
                        rigibodyGolem.AddForce(Vector3.left * 10, ForceMode.Impulse);
                        isjumping = true;
                        secondsCounter = 0;
                        secondsToCount = 5f;

                        justOne = true;
                    }
                    #endregion
                    break;
                case StateEnemyFinalOne.JumpingRightTwo:
                    #region JumpingRightTwo
                    if (!isjumping)
                    {
                        transform.Rotate(Vector3.up, -55 * Time.deltaTime);
                        if (justOne)
                        {
                            animGolem.SetTrigger("ToBacck");
                            justOne = false;
                        }
                    }
                    if (secondsCounter > secondsToCount)
                    {
                        transform.right = Vector3.forward * -1;
                        stateEnamy = StateEnemyFinalOne.Tackle;
                        secondsCounter = 0;
                        secondsToCount = 5f;
                        animGolem.SetTrigger("ReturnIdle");
                        justOne = true;
                    }
                    #endregion
                    break;
                case StateEnemyFinalOne.Tackle:
                    #region Tackle
                    if (distanceInitial > transform.position.x)
                    {
                        rigibodyGolem.velocity = new Vector3(300 * Time.deltaTime, rigibodyGolem.velocity.y, rigibodyGolem.velocity.z);
                        float velocity = Mathf.Abs(rigibodyGolem.velocity.x);
                        animGolem.SetFloat("Run", velocity);
                        secondsCounter = 0;
                        secondsToCount = 5f;
                    }
                    else
                    {

                        stateEnamy = StateEnemyFinalOne.Confuse;
                        animGolem.SetFloat("Run", 0);
                        secondsCounter = 0;
                        secondsCounter = 0;
                        secondsToCount = 5f;
                    }
                    #endregion
                    break;

                case StateEnemyFinalOne.Confuse:
                    #region Confuse
                    animatorStateInfoGolem = animGolem.GetCurrentAnimatorStateInfo(0);

                    if (justOne)
                    {
                        animGolem.SetTrigger("GetHit");
                        justOne = false;
                    }
                    if (LifeTotal <= 0 && !animatorStateInfoGolem.IsName("GetHit"))
                    {
                        stateEnamy = StateEnemyFinalOne.Die;
                        secondsCounter = 0;
                        justOne = true;
                    }
                    else if (LifeTotal > 0 && !animatorStateInfoGolem.IsName("GetHit"))
                    {
                        stateEnamy = StateEnemyFinalOne.BackToIdle;
                        secondsCounter = 0;
                        secondsToCount = 3f;
                    }
                    #endregion
                    break;
                case StateEnemyFinalOne.BackToIdle:
                    #region Confuse
                    if (!isjumping && secondsCounter < secondsToCount)
                    {
                        transform.Rotate(Vector3.up, 55 * Time.deltaTime);
                        transform.position = new Vector3(distanceInitial, transform.position.y, transform.position.z);
                        if (justOne)
                        {
                            animGolem.SetTrigger("ToBacck");
                            justOne = false;
                        }

                    }
                    else
                    {
                        transform.right = Vector3.forward;
                        stateEnamy = StateEnemyFinalOne.Idle;
                        secondsCounter = 0;
                        secondsToCount = 2f;
                    }

                    #endregion
                    break;
                case StateEnemyFinalOne.Die:
                    break;
                default:
                    break;


            }
        }
        
        
        
	}
    void OnCollisionEnter(Collision obj)
    {
        if (obj.gameObject.layer == 8)
            isjumping = false;

    }



}
