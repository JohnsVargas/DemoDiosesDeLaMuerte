﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaformaMoviles : MonoBehaviour {
    public bool move;
    float positionInitial;
    float positionFinal;
    public float distanceToMove;
    Vector3 translateMove;
    public float moveSpeed;
    public bool Vertical;
    Transform PlayerTrannform;
	// Use this for initialization
	void Start () {
        positionInitial = Vertical? transform.position.y: transform.position.x;
        positionFinal = (Vertical ? transform.position.y : transform.position.x ) + distanceToMove;
        PlayerTrannform = GameObject.Find("PlayerCapsule").transform;
    }
	
	// Update is called once per frame
	void Update () {
        if (move && Vector3.Distance(PlayerTrannform.position,transform.position)<35)
        {
            if (Vertical)
            {
                if (transform.position.y > positionFinal)
                    translateMove = Vector3.up * moveSpeed * Time.deltaTime;
                else if (transform.position.y <= positionInitial)
                    translateMove = Vector3.down * moveSpeed * Time.deltaTime;
            }
            else
            {
                if (transform.position.x > positionFinal)
                    translateMove = Vector3.back * moveSpeed * Time.deltaTime;
                else if (transform.position.x <= positionInitial)
                    translateMove = Vector3.forward * moveSpeed * Time.deltaTime;
            }
           
            transform.Translate(translateMove);
        }
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        

            collision.transform.parent = this.gameObject.transform;

    }
    private void OnCollisionExit(Collision collision)
    {

            collision.transform.parent = null;
    
    }
}
