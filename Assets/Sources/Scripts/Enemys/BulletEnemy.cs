﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletEnemy : MonoBehaviour {
    private float posCurrent , posEnd;
    // Update is called once per frame
    void Update() {
        if (this.isActiveAndEnabled) {
            if (posCurrent < posEnd)
                ObjectPool.instance.PoolGameObject(this.gameObject);
            posCurrent = transform.position.z;
        }
           
	}
    public void SetPositionInitial(float value)
    {
        posCurrent = value;
        posEnd = value - 15;
    }
}
