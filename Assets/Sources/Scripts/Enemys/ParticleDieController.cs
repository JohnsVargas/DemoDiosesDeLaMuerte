﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleDieController : MonoBehaviour {

    float secondsCounter = 0;
    float secondsToCount = 1.5f;

	void Update () {
        secondsCounter += Time.deltaTime;
        if (secondsCounter >= secondsToCount)
        {
            secondsCounter = 0;
            GetComponent<ParticleSystem>().Stop();
            ObjectPool.instance.PoolGameObject(this.gameObject);
        }
	}
    public void ResetParticleDie() {
        GetComponent<ParticleSystem>().Play();
        secondsCounter = 0;
    }
}
