﻿using UnityEngine;

public class EnemysTrampas : MonoBehaviour {

    public Transform transformPlayer;
    public float distance;
    //public bool isActive = false;
    Animator animTrampa;
    float temp;
    // Use this for initialization
    void Start () {
        animTrampa = GetComponent<Animator>();
        transformPlayer = GameObject.Find("PlayerCapsule").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update() {
        distance = transformPlayer.position.x - transform.position.x;
        distance = Mathf.Abs(distance);
        if (distance < 15)
            animTrampa.SetBool("Activado", true);
        else
            animTrampa.SetBool("Activado", false);

    }
}
