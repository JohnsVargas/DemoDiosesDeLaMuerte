﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamage : MonoBehaviour {
    public struct Enemy
    {
        public int Nivel;
        public float Damage;
    }
    Enemy enemyDetails;

    public void SetEnemyDetails(int Nivel,float Damage)
    {
        enemyDetails.Nivel = Nivel;
        enemyDetails.Damage = Damage;
    }
    public float GetDamageToPlayer() {
        return enemyDetails.Nivel * enemyDetails.Damage;
    }
}
