﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyOneController : MonoBehaviour {
	RaycastHit rayhit;
	public float distance;
    public Image lifebar;
    float life,lifenew;
    public float secondsCounter = 0;
    float secondsToCount = 0.7f;
    public enum stateEnemys {Idle,Attack,Confuse,Die };
    public enum TypeEnemy {Normal,Tackle};
    public TypeEnemy typeEnemy;
    public stateEnemys currentState;
    GameObject particleDie;
    AnimatorStateInfo animatorStateInfoPlayer;
    Rigidbody rigiBodyEnemy;
    public float move;
    //AttackPlayer
    public GameObject spear;
    private float Damage;
    public LayerMask layerMask;
    public bool flip;
    public Transform enemyBody;
   public Transform PlayerTransform = null;
    // Update is called once per frame
    private void Start()
    {
        ResetEnemy();
    }
    private void ResetEnemy() {
        //Get for Json
        lifenew = life = 2;
        currentState = stateEnemys.Idle;
        rigiBodyEnemy = GetComponent<Rigidbody>();
        Damage = 0.5f;
        spear.GetComponent<EnemyDamage>().SetEnemyDetails(1,Damage);
        
    }
    void Update () {
        switch (typeEnemy)
        {
            case TypeEnemy.Normal:
                BehavoirNormal();
                break;
            case TypeEnemy.Tackle:
                BehavoirTackle();
                break;

        }
	}

    public void BehavoirNormal()
    {
        if (currentState != stateEnemys.Die)
        {
            
            Vector3 pos = transform.position + new Vector3(0f, 1.5f, 0f);
            
            
            secondsCounter += Time.deltaTime;

            if (PlayerTransform != null)
            {
                distance = Vector3.Distance(PlayerTransform.position, transform.position);
              
                if (PlayerTransform.position.x-transform.position.x < 0)
                {

                    enemyBody.rotation = Quaternion.Euler(0, 270, 0);
                }
                else
                {
                    flip = false;
                    transform.rotation = Quaternion.Euler(0, 90, 0);
                    //transform.right = Vector3.left;
                }
            }
            else
                distance = 35;
            if (Physics.Raycast(pos, transform.forward, out rayhit,layerMask))
            {
                

                if (rayhit.transform.CompareTag("Player"))
                {
                    PlayerTransform = rayhit.transform;
                    
                   
                    
                    if (Mathf.Abs(distance) < 30 && secondsCounter >= secondsToCount)
                    {
                        //Debug.DrawLine(pos, PlayerTransform.position, Color.red);
                        currentState = stateEnemys.Attack;
                        GetComponent<Animator>().SetBool("Attack", true);
                        secondsCounter = 0;
                    }
                    else
                    {
                        currentState = stateEnemys.Idle;
                        GetComponent<Animator>().SetBool("Attack", false);
                    }

                }

            }
        }
        else if (currentState == stateEnemys.Die)
        {
            particleDie = ObjectPool.instance.GetGameObjectOffType("ParticleDie", true);
            if (particleDie != null)
            {
                particleDie.GetComponent<ParticleDieController>().ResetParticleDie();
                particleDie.transform.position = transform.position + Vector3.up;
                particleDie.transform.rotation = Quaternion.identity;
            }
            this.transform.gameObject.SetActive(false);
        }
    }
    public void BehavoirTackle()
    {
        if (currentState != stateEnemys.Die)
        {
            Vector3 pos = transform.position + new Vector3(0f, 0.5f, 0f);
            // Transform PlayerTransform;
            if (PlayerTransform != null)
            {
                distance = Vector3.Distance(PlayerTransform.position, transform.position);

                //if (PlayerTransform.position.x - transform.position.x < 0)
                //{
                //    enemyBody.rotation = Quaternion.Euler(0, 270, 0);
                //}
                //else
                //{
                //    transform.rotation = Quaternion.Euler(0, 90, 0);
                //}
            }
            else
                distance = 35;
            if (Physics.Raycast(pos, transform.forward, out rayhit) && TypeEnemy.Tackle == typeEnemy)
            {
                if (rayhit.transform.gameObject.name == "PlayerCapsule")
                {
                    PlayerTransform = rayhit.transform;
                    distance = Vector3.Distance(PlayerTransform.position, transform.position);
                    GetComponent<Animator>().SetFloat("Distance", distance);
                }
                animatorStateInfoPlayer = GetComponent<Animator>().GetCurrentAnimatorStateInfo(0);
            }
                
            if (animatorStateInfoPlayer.IsName("Run"))
            {
                rigiBodyEnemy.velocity = new Vector3(move * Time.deltaTime, rigiBodyEnemy.velocity.y, rigiBodyEnemy.velocity.z);
            }
        }
        else if (currentState == stateEnemys.Die)
        {
            particleDie = ObjectPool.instance.GetGameObjectOffType("ParticleDie", true);
            if (particleDie != null)
            {
                particleDie.GetComponent<ParticleDieController>().ResetParticleDie();
                particleDie.transform.position = transform.position + Vector3.up;
                particleDie.transform.rotation = Quaternion.identity;
            }
            this.transform.gameObject.SetActive(false);
        }
    }

    public void UpdateLifeEnemy(float damgae) {
        lifenew = lifenew - damgae;
        lifebar.fillAmount = (lifenew / life);
        if (lifebar.fillAmount <= 0.5 && currentState != stateEnemys.Die)
            lifebar.color = Color.red;
        if (lifebar.fillAmount == 0)
            currentState = stateEnemys.Die;
        //lifebar.fillAmount = (lifebar.fillAmount * damgae) / life; 
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.gameObject.CompareTag("army"))
        {
            GetComponent<Animator>().SetTrigger("Hit");
            UpdateLifeEnemy(other.GetComponent<DemagePlayer>().Damage);
            secondsCounter = 9;
        }
    }
    public float GetDamageEnemy()
    {
        return Damage;
    }
}
