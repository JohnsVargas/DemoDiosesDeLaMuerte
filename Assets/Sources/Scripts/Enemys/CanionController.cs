﻿using UnityEngine;

public class CanionController : MonoBehaviour {


    GameObject Bullet;
    public Transform transformPlayer;
    public float distance;
    private float secondsToStart;
    private float secondsToCount;
    public bool isActive = false;
    float speed;

    void Start()
    {
        speed = -8f;
        secondsToCount = 2.0f;
        secondsToStart = (Random.Range(1.0f,5.0f)/5.0f);
        transformPlayer = GameObject.Find("PlayerCapsule").GetComponent<Transform>();
        InvokeRepeating("CreateBullet", secondsToStart, secondsToCount);
    }
    private void Update()
    {
        distance = transformPlayer.position.x - transform.position.x;
        distance = Mathf.Abs(distance);
        if (distance < 15)
            isActive = true;
        else
            isActive = false;
    }
    void CreateBullet()
    {
        if (isActive)
        {
            Bullet = ObjectPool.instance.GetGameObjectOffType("Bala", true);
            Bullet.transform.position = transform.position;
            Bullet.transform.forward = transform.forward;
            Bullet.GetComponent<BulletEnemy>().SetPositionInitial(Bullet.transform.position.z);
            Bullet.GetComponent<Rigidbody>().AddForce(transform.forward * speed, ForceMode.Impulse);

        }
        
    }
}